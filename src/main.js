import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";

document.querySelector('.mainMenu li').setAttribute('href', '/');
document
	.querySelector('.mainMenu li')
	.setAttribute('class', 'pizzaListLink active');
console.log(document.querySelector('.active'));

document.querySelector('.newsContainer').setAttribute('style', 'display');

const closeButton = document.querySelector('.closeButton');

closeButton.addEventListener('click', event => {
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});

Router.menuElement = document.querySelector('.mainMenu');
