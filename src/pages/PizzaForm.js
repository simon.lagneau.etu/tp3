import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('form');
		form.addEventListener('submit', event => this.submit(event));
	}

	submit(event) {
		event.preventDefault();
		const nameInput = this.element.querySelector('input[name=name]');
		let name = nameInput.value;
		if (!name) alert('Pas de nom vide fréro');
		else alert(`C'est bon !! Ta pizza ${name} est créée`);
	}
}
